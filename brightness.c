#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <ddccontrol/conf.h>
#include <ddccontrol/ddcci.h>

#define DEFAULT_DDCCI_ADDR	0x37	/* ddc/ci logic sits at 0x37 */
#define DDCCI_CTRL_BRIGHTNESS	0x10

int main (int argc, char* argv[]) {

  if (argc <= 2) {
    fprintf(stderr, "Expected arguments: <device> <brightness>\n");
    return EXIT_FAILURE;
  }

  struct monitor mon;
  memset(&mon, 0, sizeof(struct monitor));

  if ((mon.fd = open(argv[1], O_RDWR)) < 0) {
    fprintf(stderr, "Could not open device file\n");
    return EXIT_FAILURE;
  }
  mon.type = dev;
  mon.addr = DEFAULT_DDCCI_ADDR;
  
  unsigned short val, max_val;
  ddcci_readctrl(&mon, DDCCI_CTRL_BRIGHTNESS, &val, &max_val);

  int new_val = atoi(argv[2]);
  if (argv[2][0] == '+') {
    if (val + new_val <= max_val) val += new_val;
    else val = max_val;
  }
  else if (new_val < 0) {
    if (val >= -new_val) val += new_val;
    else val = 0;
  }
  else {
    val = new_val;
  }
  
  ddcci_writectrl(&mon, DDCCI_CTRL_BRIGHTNESS, val, 0);

  close(mon.fd);
}
