brightness: brightness.c
	gcc -Wall `pkg-config --cflags --libs ddccontrol libxml-2.0` -o brightness brightness.c

clean:
	rm brightness
