#!/usr/bin/env python3

import subprocess
import re
import sys


def get_brightness_and_max():
    s = subprocess.check_output("ddcutil -b 3 --nodetect -y 1 --terse getvcp 10".split())
    m = re.fullmatch(r"VCP 10 C (\d+) (\d+)\n", str(s, "utf8"))
    return (int(m[1]), int(m[2]))


def set_brightness(brightness):
    subprocess.check_call("ddcutil -b 3 --nodetect --noverify -y 1 setvcp 10 {}".format(brightness).split())


def main(cmd, b):
    (brightness, max_brightness) = get_brightness_and_max()
    new_brightness = int(b)
    if b[0] == '-' or b[0] == '+':
        new_brightness += brightness
    new_brightness = max(0, min(max_brightness, new_brightness))
    if new_brightness != brightness:
        set_brightness(new_brightness)


if __name__ == '__main__':
    main(*sys.argv)
